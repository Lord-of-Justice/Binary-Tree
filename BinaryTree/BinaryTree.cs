﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BinaryTree
{
    public class BinaryTree<T> : IEnumerable<T>
    {
        /// <summary>
        /// Event that should be called when new element is added
        /// </summary>
        public event EventHandler ElementAdd;

        /// <summary>
        /// Event that should be called when element in tree is removed
        /// </summary>
        public event EventHandler ElementRemove;

        /// <summary>
        /// Defines how many elements tree contains
        /// </summary>
        public int Count 
        {
            get => throw new NotImplementedException();
            private set => throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if type T implements <see cref="IComparable<T>"/>
        /// If it does: Creates a lambda function, that calls T.CompateTo() method,
        /// and writes it in delegate field created before
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when T doesn't implement <see cref="IComparable<T>"</exception>
        public BinaryTree()
        {
        }

        /// <summary>
        /// Creates instance of tree 
        /// and writes comparer's method Compare() to delegate field created before
        /// </summary>
        /// <param name="comparer"><see cref="IComparer<T>"/></param>
        public BinaryTree(IComparer<T> comparer)
        {
        }

        /// <summary>
        /// Adds element to the tree according to comparer
        /// </summary>
        /// <param name="item">Object that should be added in tree</param>
        public void Add(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes element from tree by its reference
        /// </summary>
        /// <param name="item">Object that should be removed from tree</param>
        /// <returns>True if element was deleted succesfully, false if element wasn't found in tree</returns>
        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if tree contains element by its reference
        /// </summary>
        /// <param name="item">Object that should (or not) be found in tree</param>
        /// <returns>True if tree contains item, false if it doesn't</returns>
        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns item with the highest value
        /// </summary>
        /// <returns>The element with highest value</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty</exception> 
        public T TreeMax()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns item with the lowest value
        /// </summary>
        /// <returns>The element with lowest value</returns>
        /// <exception cref="InvalidOperationException">Thrown if tree is empty</exception>
        public T TreeMin()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Makes in-order traverse
        /// Serves as a default traverse for tree
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle</returns>
        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Makes tree traversal
        /// </summary>
        /// <param name="traverseType"><see cref="TraverseType"></param>
        /// <returns>Sequense of elements of tree according to traverse type</returns>
        public IEnumerable<T> Traverse(TraverseType traverseType)
        {
            //TODO: Return a sequense of elements of tree according to traverse type

            throw new NotImplementedException();
        }

        /// <summary>
        /// Makes in-order traverse
        /// Serves as a default traverse for tree
        /// </summary>
        /// <returns>Enumerator for iterations in foreach cycle</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    public enum TraverseType
    {
        InOrder,
        PreOrder,
        PostOrder
    }
}
